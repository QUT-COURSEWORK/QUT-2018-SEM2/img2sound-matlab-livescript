classdef hsvThreshold
    %hsvThreshold Contains a string of which colour the threshold belongs to,
    %as well as a HSV threshold that will retreive the given colour.
    
    properties
        colourString
        channel1Min
        channel1Max
        channel2Min
        channel2Max
        channel3Min
        channel3Max
    end
    
    methods
        function obj = hsvThreshold(cString, c1Min, c1Max, c2Min, c2Max, c3Min, c3Max)
            %hsvThreshold Construct an instance of this class
            obj.colourString = cString;
            obj.channel1Min = c1Min;
            obj.channel1Max = c1Max;
            obj.channel2Min = c2Min;
            obj.channel2Max = c2Max;
            obj.channel3Min = c3Min;
            obj.channel3Max = c3Max;
        end
        
        function bw = CreateMask(obj, img)
            %hsvThreshold Creates and returns a logical image from the classes stored
            %colour thresholds and an input image in RGB space
            
            % Convert the RGB image to HSV space
            I = rgb2hsv(img);
            
            % Create a mask with the classes thresholds
            sliderBW = (I(:,:,1) >= obj.channel1Min ) & (I(:,:,1) <= obj.channel1Max) & ...
            (I(:,:,2) >= obj.channel2Min ) & (I(:,:,2) <= obj.channel2Max) & ...
            (I(:,:,3) >= obj.channel3Min ) & (I(:,:,3) <= obj.channel3Max);
            bw = sliderBW;
        end
    end
end

