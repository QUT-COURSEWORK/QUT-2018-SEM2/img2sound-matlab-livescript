function [resampledSound] = resampler(inputSound,currentFrequency,newFrequency)
%RESAMPLER takes an input sound, current frequency on which it plays and
%the new frequency which it is planned to be resampled at. It then outputs
%this sound at the new frequency.
[N, D] = rat(currentFrequency/newFrequency);
resampledSound = resample(inputSound, N, D);
end

